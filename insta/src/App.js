import React from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Client as Styletron } from "styletron-engine-atomic";
import { Provider as StyletronProvider } from "styletron-react"; // Fix import statement
import Login from "./components/Login";
import SignUp from "./components/SignUp";
import CreatePostForm from "./components/CreatePostForm";
import CreateStoryForm from "./components/CreateStoryForm";
import FeedPage from "./components/FeedPage";
import UserProfile from "./components/UserProfile";
const App = () => {
 
  const engine = new Styletron();

  return (
    <StyletronProvider value={engine}>
      <Router>
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/signUp" element={<SignUp />} />
          <Route path="/feed" element={<FeedPage />} />
        <Route path="/create-post" element={<CreatePostForm />} />
        <Route path="/create-story" element={<CreateStoryForm />} />
        <Route path="/profile" element={<UserProfile />} />
        </Routes>
      </Router>
    </StyletronProvider>
  );
};

export default App;
