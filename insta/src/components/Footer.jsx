import React from 'react';
import { Button } from 'baseui/button';
import { Link } from 'react-router-dom';
import './css/Footer.css'
const Footer = () => {
  return (
    <div className="footer">
      <Link to="/create-post">
        <Button>Create Post</Button>
      </Link>
      <Link to="/create-story">
        <Button>Create Story</Button>
      </Link>
    </div>
  );
};

export default Footer;
