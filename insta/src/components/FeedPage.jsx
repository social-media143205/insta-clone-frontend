import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import PostCard from "./PostCard";
import "./css/FeedPage.css";
const FeedPage = () => {
  // Mock data for posts and stories
  const posts = [{ id: 1, content: "Post 1 content" }];

  return (
    <div className="feed-page">
      <Header />
      <div className="content">
        <div className="left-sidebar">
          {posts.map((post) => (
            <PostCard key={post.id} post={post} />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default FeedPage;
