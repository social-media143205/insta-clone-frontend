import React from "react";
import { Input } from "baseui/input";
import { Button } from "baseui/button";
import { Link } from "react-router-dom";
import "./css/Header.css";
const Header = () => {
  return (
    <div className="header">
      <Input placeholder="Search" />
      <Button>Stories</Button>
      <Button>Notification</Button>
      <Link to="/profile">
        <Button>User Profile</Button>
      </Link>
    </div>
  );
};

export default Header;
