import React from 'react';
import './css/PostCard.css'
const PostCard = ({ post }) => {
  return (
    <div className="post-card">
      {/* Display post details from the 'post' prop */}
      <p>{post.content}</p>
      {/* Add more post details */}
    </div>
  );
};

export default PostCard;
