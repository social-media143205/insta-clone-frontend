import React from 'react';
import './css/UserProfile.css'
const UserProfile = () => {
  // Fetch user details or use props to get user information
  const user = {
    name: 'John Doe',
    avatar: 'user-avatar.jpg',
  };

  return (
    <div className="user-profile">
      <img src={user.avatar} alt="User Avatar" />
      <p>{user.name}</p>
      {/* Add more user details */}
    </div>
  );
};

export default UserProfile;
