import React from 'react';
import { useForm } from 'react-hook-form';
import './css/CreateStoryForm.css'
const CreateStoryForm = () => {
  const { handleSubmit, register } = useForm();

  const onSubmit = (data) => {
    // Handle form submission logic
    console.log(data);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <label>Story Content:</label>
      <textarea {...register('storyContent')} />

      <button type="submit">Create Story</button>
    </form>
  );
};

export default CreateStoryForm;
