import React from 'react';
import './css/StoryCard.css'
const StoryCard = ({ story }) => {
  return (
    <div className="story-card">
      {/* Display story details from the 'story' prop */}
      <p>{story.content}</p>
      {/* Add more story details */}
    </div>
  );
};

export default StoryCard;
