import React, { useState } from "react";
import { useNavigate, Link as RouterLink } from "react-router-dom";
import { useForm, Controller } from "react-hook-form";
import { Button } from "baseui/button";
import { Input } from "baseui/input";
import "./css/SignUp.css";

const SignUp = () => {
  const navigate = useNavigate();
  const { handleSubmit, control, formState } = useForm();
  const { errors } = formState;
  const [isPasswordValid, setIsPasswordValid] = useState(true);
  const [userSignUp, setUserSignUp] = useState(null);

  const validPassword = (password) => {
    return /^(?=.*[A-Za-z0-9])(?=.*[!@#$%^&*()_+])[A-Za-z0-9!@#$%^&*()_+]{8,}$/.test(
      password
    );
  };

  const isEmailValid = (email) => {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  };

  const handleSignUp = async (data) => {
    console.log("Handle SignUp executed");
    try {
      if (!isEmailValid(data.email)) {
        alert("Please enter a valid email address.");
        return;
      }

      if (!validPassword(data.password)) {
        console.log("Here valid password");
        alert(
          "Password should be at least 8 characters long and contain at least one special character."
        );
        return;
      }

      setUserSignUp({
        email: data.email,
        name: data.name,
        password: data.password,
      });

      const response = await fetch("http://localhost:8081/v1.0/users/signup", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      });

      if (response.ok) {
        navigate("/");
      } else {
        alert("Error during SignUp");
      }
    } catch (error) {
      console.error("Error during SignUp:", error);
    }
  };

  return (
    <form onSubmit={handleSubmit(handleSignUp)}>
      <Controller
        name="email"
        control={control}
        defaultValue=""
        rules={{
          required: "Email is required",
          pattern: {
            value: /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
            message: "Invalid email address",
          },
        }}
        render={({ field }) => (
          <Input
            {...field}
            className="input-field"
            placeholder="Email"
            type="email"
            error={errors.email?.message || null}
          />
        )}
      />
      <Controller
        name="name"
        control={control}
        defaultValue=""
        rules={{ required: "Name is required" }}
        render={({ field }) => (
          <Input
            {...field}
            className="input-field"
            placeholder="Name"
            type="text"
            error={errors.name?.message || null}
          />
        )}
      />
      <Controller
        name="password"
        control={control}
        defaultValue=""
        rules={{
          required: "Password is required",
          pattern: {
            value:
              /^(?=.*[A-Za-z0-9])(?=.*[!@#$%^&*()_+])[A-Za-z0-9!@#$%^&*()_+]{8,}$/,
            message:
              "Password should be at least 8 characters long and contain at least one special character.",
          },
        }}
        render={({ field }) => (
          <Input
            {...field}
            className="input-field"
            placeholder="Password"
            type="password"
            onBlur={(e) => setIsPasswordValid(validPassword(e.target.value))}
            error={errors.password?.message || null}
          />
        )}
      />
      <div className="button-container">
        <Button type="submit" disabled={!isPasswordValid}>
          Sign Up
        </Button>
      </div>
      {userSignUp && (
        <div className="user-data">
          <p>Email: {userSignUp.email}</p>
          <p>Name: {userSignUp.name}</p>
          <p>Password: {userSignUp.password}</p>
        </div>
      )}
      {formState.errors.email && (
        <p className="error-message">{formState.errors.email.message}</p>
      )}
      {formState.errors.name && (
        <p className="error-message">{formState.errors.name.message}</p>
      )}
      {formState.errors.password && (
        <p className="error-message">{formState.errors.password.message}</p>
      )}
      <div className="link-container">
        <RouterLink to="/" className="login-link">
          <Button kind="secondary">Already have an account? Login</Button>
        </RouterLink>
      </div>
    </form>
  );
};

export default SignUp;
