import React from 'react';
import { useForm } from 'react-hook-form';
import  './css/CreatePostForm.css';
const CreatePostForm = () => {
  const { handleSubmit, register } = useForm();

  const onSubmit = (data) => {
    // Handle form submission logic
    console.log(data);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <label>Title:</label>
      <input type="text" {...register('title')} />

      <label>Content:</label>
      <textarea {...register('content')} />

      <button type="submit">Create Post</button>
    </form>
  );
};

export default CreatePostForm;
